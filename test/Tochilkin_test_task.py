import math

import numpy as np
import pandas as pd

data_frame = pd.read_table(
    'mtesrl_20150626_MD0000600002_stats.txt',
    skiprows=2,
    usecols=('EVENT', 'AVGTSMR'),
    dtype={'EVENT': str, 'AVGTSMR': np.float}, # np.int32 does not support NA values
).dropna()

for event in data_frame['EVENT'].unique():
    event_df = data_frame[data_frame['EVENT'] == event]
    stats = [
        event,
        event_df['AVGTSMR'].min(),
        event_df['AVGTSMR'].median(),
        event_df['AVGTSMR'].quantile(0.9),
        event_df['AVGTSMR'].quantile(0.99),
        event_df['AVGTSMR'].quantile(0.999)
    ]
    print('\n{} | min = {:>3} | 50% = {:>3} | 90% = {:>3} | 99% = {:>3} | 99.9% = {:>3}\n'.format(*stats))

    # set time as a multiple of 5 (list comprehension is faster then .apply())
    event_df['AVGTSMR'] = [math.ceil(x / 5) * 5 for x in event_df['AVGTSMR']]

    result_table = pd.DataFrame(event_df.groupby(['AVGTSMR'], as_index=False).count())
    result_table.rename(index=str, columns={'AVGTSMR': 'ExecTime', 'EVENT': 'TransNo'})

    result_table['Weight,%'] = round(100 * result_table['EVENT'] / event_df.shape[0], 2)
    result_table['Percent'] = [result_table.iloc[:i + 1]['Weight,%'].sum() for i in range(result_table.shape[0])]

    print(result_table.to_string(index=False))
    print(80 * '-')
