# from math import factorial
#
# N = 7
# p = 0.8
# m = int((N/2)+1)
#
#
# def C (k,n):
#     return factorial(n)/(factorial(k)*factorial(n-k))
#
# mu = 0
#
# for i in range(m,N+1):
#     mu+= C(i,N)*p**i*(1-p)**(N-i)
#
# print(mu)
from random import shuffle

independent_columns_names = [1,2,3,4,5,6,7]
col = independent_columns_names[:]
shuffle(col)

print(col[:5])
